#include "Vector.h"
#include <iostream>
#include <cstdio>
using namespace std;

int main()
{
	Vector v1 = Vector(5);
	v1.resize(10, 9);
	v1.print();

	v1.resize(18);
	v1.assign(4);
	v1.print();

	Vector v2 = Vector(v1);
	v2.print();
	getchar();

	return 0;
}