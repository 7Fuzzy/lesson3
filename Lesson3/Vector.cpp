#include "Vector.h"
#include <iostream>
using namespace std;


/*
	Constructor Function - Builds the vector
	input: n - capacity
	output: Vector
*/
Vector::Vector(int n)
{
	this->_capacity = n;
	this->_resizeFactor = n <= 1 ? 2 : n;
	this->_size = 0;
	this->_elements = new int[n];
}

/*
	Copy constructor - makes a copy of the given vector
	input: vector
	output: vector
*/
Vector::Vector(const Vector& v)
{
	*this = v;
}

/*
	Operator - copies a vector
	input: vector
	output: vector
*/
Vector& Vector::operator=(const Vector& other)
{
	this->_capacity = other.capacity();
	this->_resizeFactor = other.resizeFactor();
	this->_size = other.size();
	int* newArr = new int[this->_capacity];

	for (int i = 0; i < this->_size; i++)
	{
		newArr[i] = other._elements[i];
	}

	if (this->_elements != nullptr)
	{
		delete[](this->_elements);
	}
	this->_elements = newArr;

	return *this;
}

/*
	Operator - getting elements of vector according to size
	input: index
	output: int - element
*/
int& Vector::operator[](int n) const
{
	if (n >= this->_size)
	{
		cout << "Error: Index out of range" << endl;
		return this->_elements[0];
	}
	else
	{
		return this->_elements[n];
	}
}

/*
	Destructor Function - frees the memory
	input: none
	output: none (cleared)
*/
Vector::~Vector()
{
	delete _elements;
}

/*
	Getter - returns the size of the Vector
	input: none
	output: size - int
*/
int Vector::size() const
{
	return this->_size;
}

/*
	Getter - returns the capacity of the Vector
	input: none
	output: capacity - int
*/
int Vector::capacity() const
{
	return this->_capacity;
}

/*
	Getter - returns the resize factor
	input: none
	output: resize factor - int
*/
int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

/*
	Function returns if the vector is empty
	input: none
	output: empty or not - bool
*/
bool Vector::empty() const
{
	return this->_size == 0;
}

/*
	Function reserves the vector according to given capacity
	input: n - capacity
	output: none
*/
void Vector::reserve(int n)
{
	if (n > this->_capacity) //if n < capacity -> we have enough space
	{
		int toAdd = n - this->_capacity; // how many elements needs to be reserved
		int toAddFactor = toAdd / this->_resizeFactor + 1; // how many elements we can add using the factor
		int newCap = this->_capacity + toAddFactor * this->_resizeFactor;
		int* newArr = new int[newCap];

		//Copies the array
		for (int i = 0; i < this->_capacity; i++)
		{
			newArr[i] = this->_elements[i];
		}
		delete[] (this->_elements);
		
		this->_elements = newArr;
		this->_capacity = newCap;
	}
}

/*
	Function Tries to remove an element from the back of the array, returns -9999 if it doesnt work
	input: none
	output: removed element
*/
int Vector::pop_back()
{
	if (this->empty())
	{
		cout << "Error: cant remove from empty vector" << endl;
		return -9999;
	}
	else
	{
		(this->_size)--;
		int ret = this->_elements[this->_size];
		return ret;
	}
}

/*
	Function adds given number to the back of the array
	input: number
	output: none
*/
void Vector::push_back(const int& val)
{
	if (this->_size == this->_capacity)
	{
		int* newArr = new int[++(this->_capacity)]; // increaces capacity and creates new array

		for (int i = 0; i < this->_capacity; i++)
		{
			if (i == this->_capacity)
			{
				newArr[i] = val;
			}
			else
			{
				newArr[i] = this->_elements[i];
			}
		}

		delete this->_elements;
		this->_elements = newArr;
		(this->_size)++;
	}
}

/*
	Function sets all of the elements to be the given value
	input: value
	output: none
*/
void Vector::assign(int val)
{
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = val;
	}
}

/*
	Function prints the vector values
	input: none
	output: none
*/
void Vector::print()
{
	for (int i = 0; i < this->_size; i++)
	{
		cout << this->_elements[i] << ", ";
	}

	cout << endl;
}

/*
	Function resizes the array according to the given value
	input: n - new size
	output: none
*/
void Vector::resize(int n)
{
	if (n > this->_capacity)
	{
		this->reserve(n);
	}
	this->_size = n;
}

/*
	Function resizes the array and changes the elements to the given value
	input: n - new size, val - value to assaign
	output: none
*/
void Vector::resize(int n, const int& val)
{
	this->resize(n);
	this->assign(val);
}

